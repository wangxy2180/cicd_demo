FROM openjdk:11
EXPOSE 8081

WORKDIR /root
ADD jarfile/cicd_demo*.jar /root/app.jar
ENTRYPOINT ["java","-jar","/root/app.jar"]